Name: Anthony Dewa Priyasembada

NPM: 1706979171

Class: Advanced Programming B

Hobby : Sleep

Links:
- https://gitlab.com/AnthonyDP/my-first-git-repo.git
- https://gitlab.com/AnthonyDP/DDP2-assignment.git

My Notes:
- Git Branch digunakan digunakan untuk mengedit repository tetapi belum final, jika ada bug atau fitur belum terimplementasi sempurna, maka repository utama "master" tidak akan terpengaruh

- How to git branch:
    1. git branch [nama_branch]  -> membuat branch baru
    2. git checkout [nama_branch] -> menuju branch sesuai nama_branch
    3. git merge [nama_branch] -> menggabungkan branch [nama_branch] dengan branch yang sekarang, dilakukan jika edit di branch [nama_branch] sudah final

- Kapan menggunakan git revert?
    ketika terdapat bug yang sangat mempengaruhi program di repository, git revert dapat mengembalikan repository ke commit yang masih bersih dari bug, dengan membuat commit baru

- how to git revert:
    1. git revert [commit_hash], revert ke commit [commit_hash], [commit_hash] dapat dilihat dari "git log"
    2. jika terjadi conflict, fix conflictnya
    3. lakukan "git add <nama_file>"
    4. lakukan "git commit", akan muncul text editor vim, edit yang diperlukan
    5. jika sudah selesai mengedit, tutup vim
    6. lakukan "git push origin [nama_branch]" untuk mempublish perubahan

